<?php

namespace App\Service;


use App\Entity\ListItem;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ICNDBService extends ApiCallService implements ICNDBServiceInterface
{

    const API_DOMAIN = 'http://api.icndb.com/';

    /**
     * ICNDBService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container, static::API_DOMAIN);
    }




    public function getRandomJoke(int $nrOfJokes):ArrayCollection
    {
        $response = $this->client->get(
            'jokes/random/'.$nrOfJokes
        );

        $data = $response->getBody();
        $nr = 0;
        $collection = new ArrayCollection();
        foreach (json_decode($data)->value as $one) {
            $listItem = new ListItem();
            $listItem->message = $one->joke;
            $listItem->number = ++$nr;
            $collection->add($listItem);
        }

        return $collection;
    }
}