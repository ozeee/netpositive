<?php
/**
 * Created by PhpStorm.
 * User: OZ
 * Date: 2018. 12. 16.
 * Time: 15:43
 */

namespace App\Service;


use Doctrine\Common\Collections\ArrayCollection;

interface MergeServiceInterface
{
    public function MergeByAttribute(ArrayCollection $collectionOne, ArrayCollection $collectionTwo, string $attribute):ArrayCollection;

    public function AddToElementsByPosition(
        ArrayCollection $collection,
        ArrayCollection $addableCollection,
        ArrayCollection $positions
    ):ArrayCollection;
}