<?php
/**
 * Created by PhpStorm.
 * User: OZ
 * Date: 2018. 12. 14.
 * Time: 20:47
 */

namespace App\Service;


use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class ApiCallService
{
    protected $client;
    protected $container;

    /**
     * ApiCallService constructor.
     * @param ContainerInterface $container
     * @param string $baseUri
     * @param HandlerStack|null $stack
     * @param null|string $auth
     */
    public function __construct(ContainerInterface $container, string $baseUri, ?HandlerStack $stack = null, ?string $auth = null)
    {
        $this->container = $container;
        $this->client = new Client([
            'base_uri' => $baseUri,
            'handler' => $stack,
            'auth' => $auth,
            'verify' => false
        ]);
    }

}