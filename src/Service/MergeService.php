<?php


namespace App\Service;


use App\Entity\ListItem;
use Doctrine\Common\Collections\ArrayCollection;

class MergeService implements MergeServiceInterface
{
    /**
     * @param ArrayCollection $collectionOne
     * @param ArrayCollection $collectionTwo
     * @param string $attribute
     * @return ArrayCollection
     */
    public function MergeByAttribute(ArrayCollection $collectionOne, ArrayCollection $collectionTwo, string $attribute): ArrayCollection
    {
        $mergedCollection = new ArrayCollection();
        $compareA = $collectionOne->first();
        $compareB = $collectionTwo->first();
        while ($compareA || $compareB) {
            if ($compareA && $compareB) {
                if (strtotime($compareA->$attribute) >= strtotime($compareB->$attribute)) {
                    $mergedCollection->add($compareA);
                    $compareA = $collectionOne->next();
                } else {
                    $mergedCollection->add($compareB);
                    $compareB = $collectionTwo->next();
                }
            } elseif (!$compareA) {
                $mergedCollection->add($compareB);
                $compareB = $collectionTwo->next();
            } elseif (!$compareB) {
                $mergedCollection->add($compareA);
                $compareA = $collectionOne->next();
            }
        }
        return $mergedCollection;
    }

    public function AddToElementsByPosition(
        ArrayCollection $collection,
        ArrayCollection $addableCollection,
        ArrayCollection $positions
    ):ArrayCollection
    {
        $newCollection = new ArrayCollection();
        $newCollectionPosition = 1;
        foreach ($collection->getIterator() as $key => $item) {

            if ($positions->contains($newCollectionPosition)) {
                $newCollection->add($addableCollection->next());
                $newCollectionPosition++;
            }
            $newCollection->add($item);
            $newCollectionPosition++;
        }

        return $newCollection;
    }
}