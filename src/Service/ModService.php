<?php


namespace App\Service;


use Doctrine\Common\Collections\ArrayCollection;

class ModService implements ModServiceInterface
{
    const MOD_PARAM = 3;
    const FIB_MORE_THAN = 2;
    private $fibSeries;

    public function modParam($n):ArrayCollection
    {
        $cache = new ArrayCollection();
        for ($it=1;$it<=$n*static::MOD_PARAM;$it++) {
            if ($it % static::MOD_PARAM == 0) {
                $cache->add($it);
            }
        }
        return $cache;
    }

    public function fibParam($n):ArrayCollection
    {
        $this->clearFibSeries();
        $this->createFibSeries($n);
        return $this->getFibSeries()->filter(function($i) {
            return $i > static::FIB_MORE_THAN;
        });
    }

    private function createFibSeries($n):int
    {
        if ($n < 0) {
            return NULL;
        } elseif ($n === 0) {
            $this->fibSeries[$n] = 0;
            return 0;
        } elseif ($n === 1 || $n === 2) {
            $this->fibSeries[$n] = 1;
            return 1;
        } else {
            if (!(!is_null($this->fibSeries) && array_key_exists($n, $this->fibSeries))) {
                $this->fibSeries[$n] = $this->createFibSeries($n-1) + $this->createFibSeries($n-2);
            }
            return $this->fibSeries[$n];
        }
    }

    /**
     * @return ArrayCollection
     */
    private function getFibSeries():ArrayCollection
    {
        return new ArrayCollection($this->fibSeries);
    }

    /**
     *
     */
    private function clearFibSeries():void
    {
        $this->fibSeries = [];
    }
}