<?php
/**
 * Created by PhpStorm.
 * User: OZ
 * Date: 2018. 12. 14.
 * Time: 11:40
 */

namespace App\Service;


use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Client;

interface TwitterServiceInterface
{
    public function getUserTimeline($userID, $limit):ArrayCollection;
}