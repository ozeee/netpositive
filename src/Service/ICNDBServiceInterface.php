<?php


namespace App\Service;


use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Client;

interface ICNDBServiceInterface
{
    public function getRandomJoke(int $nrOfJokes):ArrayCollection;

}