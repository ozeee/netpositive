<?php

namespace App\Service;

use App\Entity\ListItem;
use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Symfony\Component\DependencyInjection\ContainerInterface;



class TwitterService extends ApiCallService implements TwitterServiceInterface
{

    const TIMELINE_URI = 'statuses/user_timeline.json';
    const AUTH_TYPE = 'oauth';
    const API_DOMAIN = 'https://api.twitter.com/1.1/';



    /**
     * TwitterService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {

        $stack = HandlerStack::create();

        $middleware = new Oauth1([
            'consumer_key'    => $container->getParameter('twitter')['consumer_key'],
            'consumer_secret' => $container->getParameter('twitter')['consumer_secret'],
            'token'           => $container->getParameter('twitter')['token'],
            'token_secret'    => $container->getParameter('twitter')['secret']
        ]);
        $stack->push($middleware);
        parent::__construct($container, static::API_DOMAIN, $stack, static::AUTH_TYPE);

    }


    public function getUserTimeline($userID, $limit):ArrayCollection
    {
        $response = $this->client->get(
            static::TIMELINE_URI,['query' => [
                'screen_name' => $userID,
                'count' => $limit
            ]]
        );

        $data = $response->getBody();

        $nr = 0;

        $collection = new ArrayCollection();

        foreach (json_decode($data) as $one) {

            $listItem = new ListItem();
            $listItem->time = $one->created_at;
            $listItem->message = $one->text;
            $listItem->number = ++$nr;
            $listItem->source = $one->user->screen_name;
            $collection->add($listItem);
        }

        return $collection;
    }

}