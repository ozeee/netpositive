<?php
/**
 * Created by PhpStorm.
 * User: OZ
 * Date: 2018. 12. 16.
 * Time: 15:36
 */

namespace App\Service;


use Doctrine\Common\Collections\ArrayCollection;

interface ModServiceInterface
{
    public function fibParam($n):ArrayCollection;
    public function modParam($n):ArrayCollection;
}