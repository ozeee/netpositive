<?php

namespace App\Controller;

use App\Kernel;
use App\Service\ICNDBServiceInterface;
use App\Service\MergeServiceInterface;
use App\Service\ModServiceInterface;
use App\Service\TwitterServiceInterface;
use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class MergeAndMixController extends AbstractController
{

    const LIMIT = 5;
    const MOD_PARAM = 'mod';
    const FIB_PARAM = 'fib';
    const NR_OF_ELEMENTS_OF_PARAM = 20;
    const MERGE_ATTRIBUTE = 'time';
    private $twitterService;
    private $ICNDBService;
    private $modService;
    private $mergeService;

    /**
     * MergeAndMixController constructor.
     * @param TwitterServiceInterface $twitterService
     * @param ICNDBServiceInterface $ICNDBService
     * @param ModServiceInterface $modService
     * @param MergeServiceInterface $mergeService
     */
    public function __construct(
        TwitterServiceInterface $twitterService,
        ICNDBServiceInterface $ICNDBService,
        ModServiceInterface $modService,
        MergeServiceInterface $mergeService
    )
    {
        $this->twitterService = $twitterService;
        $this->ICNDBService = $ICNDBService;
        $this->modService = $modService;
        $this->mergeService = $mergeService;
    }

    /**
     * @Route("/{handle1}/{handle2}/{mod}", name="merge_and_mix", defaults={"mod":"fib"})
     * @param $handle1
     * @param $handle2
     * @param $mod
     *
     * @return Response
     */
    public function index($handle1, $handle2, $mod):Response
    {

        if ($handle1 == $handle2) {
            throw new BadRequestHttpException('Please type two different name of twitter feed', null, 400);
        }
        /** @var ArrayCollection $twitterFeed1 */
        $twitterFeed1 = $this->twitterService->getUserTimeline($handle1, static::LIMIT);
        /** @var ArrayCollection $twitterFeed2 */
        $twitterFeed2 = $this->twitterService->getUserTimeline($handle2, static::LIMIT);

        /** @var ArrayCollection $randomJokes */
        $randomJokes = $this->ICNDBService->getRandomJoke(5);

        $jokesInjectionList = new ArrayCollection();

        switch ($mod) {
            case static::MOD_PARAM:
                $jokesInjectionList = $this->modService->modParam(static::NR_OF_ELEMENTS_OF_PARAM);
                break;
            case static::FIB_PARAM:
                $jokesInjectionList = $this->modService->fibParam(static::NR_OF_ELEMENTS_OF_PARAM);
                break;
        }

        $mergedTwitterFeed = $this->mergeService->MergeByAttribute($twitterFeed1, $twitterFeed2, static::MERGE_ATTRIBUTE);

        /** @var ArrayCollection $finalList */
        $finalList = $this->mergeService->AddToElementsByPosition(
            $mergedTwitterFeed,
            $randomJokes,
            $jokesInjectionList
        );

        return $this->render('feed.html.twig', ['finalList' => $finalList]);
    }
}
